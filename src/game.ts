////////////////////////////////////////////
// Demonstrates 3 animations running at the same time on one model.
// Because the animations affect the same bones, the weights on the three must add up to <= 1.0, which they do.
// The animations are bend, twist and swing.
// The 3 boxes can turn the dindividual animations off and back on.
// The speeds of the three animations are not rationally related (1, PI, and 1/SQRT(2)), so the animation is ever-changing.
// -- Carl Fravel
////////////////////////////////////////////

let column = new Entity()
//column.addComponent(new GLTFShape("models/TwistyColumn/TwistyColumn3.glb"))
//column.addComponent(new GLTFShape("models/TwistyColumn/TwistyColumn3FBX.glb"))
column.addComponent(new GLTFShape("models/TwistyColumn/TwistyColumn3Sampled.glb"))
column.addComponent(new Transform({position: new Vector3(8, 0, 8)}))

let animator = new Animator()
column.addComponent(animator)

const clipBend = new AnimationState("bend")
//const clipBend = new AnimationState("Armature|Armature|bend|Armature|bend")
clipBend.playing = true
clipBend.looping = true
clipBend.weight = 0.34
clipBend.speed = 1
animator.addClip(clipBend)

const clipSwing = new AnimationState("swing")
//const clipSwing = new AnimationState("Armature|Armature|swing|Armature|swing")
clipSwing.playing = true
clipSwing.looping = true
clipSwing.weight = 0.33
clipSwing.speed = 1/Math.SQRT2
animator.addClip(clipSwing)

const clipTwist = new AnimationState("twist")
//const clipTwist = new AnimationState("Armature|Armature|twist|Armature|twist")
clipTwist.playing = true
clipTwist.looping = true
clipTwist.weight = 0.33
clipTwist.speed = Math.PI
animator.addClip(clipTwist)

engine.addEntity(column)


let buttonBox = new Entity()
buttonBox.addComponent(new BoxShape())
buttonBox.addComponent(new Transform({position: new Vector3(2, 1, 2)}))
engine.addEntity(buttonBox)

buttonBox.addComponent(new OnClick(()=>{
    if (clipBend.playing) {
        //clipBend.stop()
        clipBend.playing=false
    }
    else{
        //clipBend.play()
        clipBend.playing=true
    }
}))

let buttonBox2 = new Entity()
buttonBox2.addComponent(new BoxShape())
buttonBox2.addComponent(new Transform({position: new Vector3(4, 1, 2)}))
engine.addEntity(buttonBox2)

buttonBox2.addComponent(new OnClick(()=>{
    if (clipSwing.playing) {
        //clipSwing.stop()
        clipSwing.playing=false
    }
    else{
        //clipSwing.play()
        clipSwing.playing=true
    }
}))

let buttonBox3 = new Entity()
buttonBox3.addComponent(new BoxShape())
buttonBox3.addComponent(new Transform({position: new Vector3(6, 1, 2)}))
engine.addEntity(buttonBox3)

buttonBox3.addComponent(new OnClick(()=>{
    if (clipTwist.playing) {
        //clipTwist.stop()
        clipTwist.playing=false
    }
    else{
        //clipTwist.play()
        clipTwist.playing=true
    }
}))
