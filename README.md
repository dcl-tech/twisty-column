Demonstrates 3 animations running at the same time on one model.

Because the animations affect the same bones, the weights on the three must add up to <= 1.0, which they do.

The animations are bend, twist and swing.

The 3 boxes can turn the individual animations off and back on.

The speeds of the three animations are not rationally related (1, PI, and 1/SQRT(2)), so the animation is ever-changing.